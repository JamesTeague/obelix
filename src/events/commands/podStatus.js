// @flow
import type { Core_v1Api } from '@kubernetes/client-node';
import type { Message } from 'discord.js';
import moment from 'moment';
import { createMessageListener } from '../../packages/messageListener';
import KubernetesPhase from '../enums/KubernetesPhase';
import embedBuilder from '../../util/embedBuilder';
import pods from '../../util/pods';

const description = 'The phase of a Pod is a simple, high-level summary of where the Pod is in its lifecycle. ' +
  'The phase is not intended to be a comprehensive rollup of observations of Container or Pod state, ' +
  'nor is it intended to be a comprehensive state machine.';

const getEmbedColor = (phase: string): number => {
  const greenRegex = new RegExp('('+ KubernetesPhase.RUNNING + '|' + KubernetesPhase.SUCCEEDED + ')');
  const yellowRegex = new RegExp('('+ KubernetesPhase.PENDING + '|' + KubernetesPhase.UNKNOWN + ')');

  if (phase.match(greenRegex)) {
    return 0x7cfc00;
  } else if (phase.match(yellowRegex)) {
    return 0xffbf00;
  }
  return 0xff0000;
};

const podStatus = (message: Message, k8sApi: Core_v1Api) => {
  const args = message.content.split(' ');

  if (!args[1]) {
    message.channel.send('You didn\'t give me a pod name. Cannot get status.');
  } else {
    k8sApi.readNamespacedPodStatus(args[1], 'default', true)
      .then((res) => {
        const status = res.body.status;
        const phase = status.phase;
        const richEmbed = embedBuilder.createEmbed(
          'Pod status',
          { name: process.env.BOT_NAME || '', iconUrl: process.env.BOT_ICON_URL || '' },
          getEmbedColor(phase),
          description,
        );

        richEmbed.addField('Name', status.containerStatuses[0].name);
        richEmbed.addField('Phase', phase);
        richEmbed.addField('Start Time:', moment(status.startTime).toDate());
        richEmbed.addField('Version:', pods.getImageVersion(status.containerStatuses[0].image));

        message.channel.send({embed: richEmbed}).then(() => message.delete());
      })
      .catch(error => message.channel.send(embedBuilder.createK8sErrorEmbed(error, { name: process.env.BOT_NAME || '', iconUrl: process.env.BOT_ICON_URL || '' })));
  }
};

export default createMessageListener('*podStatus', podStatus);