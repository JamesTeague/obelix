// @flow
import { createMessageListenerPack} from '../packages/messageListener';
import removeRole from './commands/removeRole';
import grantRole from './commands/grantRole';
import deletePod from './commands/deletePod';
import podStatus from './commands/podStatus'

export default createMessageListenerPack([
  deletePod,
  grantRole,
  podStatus,
  removeRole,
]);
