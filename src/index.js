import dotenv from 'dotenv';
import Discord from 'discord.js';
import { Core_v1Api, KubeConfig } from '@kubernetes/client-node';
import messageListeners from './events';

dotenv.config();
const client = new Discord.Client();

/*
* Kubernetes Config
*/
const cluster = {
  name: process.env.K8S_NAME,
  server: process.env.K8S_CLUSTER_SERVER,
  caData: process.env.K8S_CLUSTER_CADATA,
  skipTLSVerify: true,
};

const user = {
  name: process.env.K8S_USER_NAME,
  token: process.env.K8S_TOKEN,
};

const context = {
  name: process.env.K8S_NAME,
  user: user.name,
  cluster: cluster.name,
};

const kc = new KubeConfig();
kc.loadFromOptions({
  clusters: [cluster],
  users: [user],
  contexts: [context],
  currentContext: context.name,
});
const k8sApi = kc.makeApiClient(Core_v1Api);

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', (message) => {
  if (message.content.startsWith('*')) {
    const command = message.content.split(' ')[0];
    const listener = messageListeners[command];

    if(listener) {
      listener(message, k8sApi);
    }
  }
});

client.login(process.env.BOT_TOKEN);
