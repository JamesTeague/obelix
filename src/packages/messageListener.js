// @flow
import type { Message } from 'discord.js';

type MessageListenerFunc = (message: Message) => void;
type MessageListener = { action: string, func: MessageListenerFunc };
type MessageListenerPack = { [string]: MessageListener }

const createMessageListener = (action: string, func: MessageListenerFunc) => {
  if (!action) {
    throw new Error('Action is required');
  }
  return { action, func };
};

const createMessageListenerPack = (processors: Array<MessageListener>): MessageListenerPack =>
  processors.reduce((acc, proc) => ({
    ...acc,
    [proc.action]: proc.func,
  }), {});

export {
  createMessageListener,
  createMessageListenerPack,
};
