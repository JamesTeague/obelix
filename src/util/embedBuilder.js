// @flow
import { RichEmbed } from 'discord.js';
import type { V1Pod } from '@kubernetes/client-node';

const createEmbed = (
  title: string,
  author: { name: string, iconUrl: string },
  color: number,
  description: string
): RichEmbed => {
  const richEmbed = new RichEmbed();
  richEmbed.setTitle(title)
    .setAuthor(author.name, author.iconUrl)
    .setColor(color)
    .setDescription(description);
  return richEmbed;
};

const createK8sErrorEmbed = (error: V1Pod, author: { name: string, iconUrl: string }) => {
  return new RichEmbed()
    .setTitle('Error')
    .setAuthor(author.name, author.iconUrl)
    .setColor(0xff0000)
    .setDescription(error.body.message);
};

export default {
  createEmbed,
  createK8sErrorEmbed,
};