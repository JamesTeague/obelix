// @flow

const getImageVersion = (imageTag: string) => imageTag.split(':')[1];

export default {
  getImageVersion
};
